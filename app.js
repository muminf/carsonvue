const car = (id,name, model, owner, year, phone, image) => ({id, name, model, owner, year, phone, image})
const log = (text, type, date = new Date()) => ({text, type, date})
function getById(cars){
  return cars.filter(car => {
    return car.id === index
  })
}
const cars = [
  car(1,'Ford','Focus','Max',2016, '+992 92 640 90 00', 'images/focus.jpg'),
  car(2,'Ford','Mondeo','Abdu',2018, '+992 92 640 90 00', 'images/p1.jpg'),
  car(3,'Porche','Panamera','Jabbor',2015, '+992 92 640 90 00', 'images/porche.jpg')
]
console.log(cars)
new Vue({
  el: '#app',
  data:{
    cars: cars,
    car: cars[0],
    logs: [],
    selectedCarIndex: 0,
    showPhone: false,
    search: '',
    modalVisibility: false
  },
  methods: {
    selectCar: function (index) {
      this.car = this.cars.find(c => c.id === Number(index));
      this.selectedCarIndex = this.car.id
    },
    newOrder() {
      this.modalVisibility = false
      this.logs.push(
        log(`Success order: ${this.car.name} - ${this.car.model}`,'ok')
      )
    },
    cancelOrder() {
      this.modalVisibility = false
      this.logs.push(
        log(`Cancelled order: ${this.car.name} - ${this.car.model}`,'cancel')
      )
    }
  }, 
  computed: {
    phoneBtnText() {
      return this.showPhone ? 'Hide phone' : 'Show phone'
    },
    filteredCars() {
      return this.cars.filter(car => {
        return car.name.indexOf(this.search) > -1 || car.model.indexOf(this.search) > -1
      })
    }
  },
  filters: {
    date(value) {
      return value.toLocaleString()
    }
  }
})